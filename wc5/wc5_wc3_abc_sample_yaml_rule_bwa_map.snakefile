PATHTOFILES = '/commons/Themas/Thema11/Dataprocessing/data'
rule bwa_map:
    input:
        "{}/genome.fa".format(PATHTOFILES),
        "%s/samples/{sample}.fastq" % (PATHTOFILES)
    output:
        "mapped_reads/{sample}.bam"
    benchmark:
        "benchmarks/{sample}.bwa.benchmark.txt"
    log:
        "logs/bwa_mem/{sample}.log"
    threads: 3
    message: "executing bwa mem on the following {input} to generate the following {output}"
    shell:
        "bwa mem -t{threads} {input} | samtools view -Sb - > {output}"
