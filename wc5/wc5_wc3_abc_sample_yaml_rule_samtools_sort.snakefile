rule samtools_sort:
    input:
        "mapped_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam"
    log:
        "logs/samtools_sort/{sample}.log"
    message:"sorting {input} and converting to {output}"
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} -O bam {input} > {output}"
