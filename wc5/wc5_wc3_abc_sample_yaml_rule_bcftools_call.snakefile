PATHTOFILES = '/commons/Themas/Thema11/Dataprocessing/data'
rule bcftools_call:
    input:
        fa="%s/genome.fa" % (PATHTOFILES),
        bam=expand("sorted_reads/{sample}.bam", sample=config["samples"]),
        bai=expand("sorted_reads/{sample}.bam.bai", sample=config["samples"])
    output:
        "calls/all.vcf"
    log:
        expand("logs/bcftools_call/{sample}.log",sample=config["samples"])
    message:" run pileup with {input.fa} and {input.bam} and write to {output}"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv - > {output}"
