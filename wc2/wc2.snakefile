SAMPLES = ['A','B','C']
PATHTOFILES = '/commons/Themas/Thema11/Dataprocessing/data'

rule all:
	input:
		'out.html'

rule bwa_map:
    input:
        "{}/genome.fa".format(PATHTOFILES),
        "%s/samples/{sample}.fastq" % (PATHTOFILES)
    output:
        "mapped_reads/{sample}.bam"
    message: "executing bwa mem on the following {input} to generate the following {output}"
    shell:
        "bwa mem {input} | samtools view -Sb - > {output}"

rule samtools_sort:
    input:
        "mapped_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam"
    message:"sorting {input} and converting to {output}"
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} -O bam {input} > {output}"


rule samtools_index:
    input:
        "sorted_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam.bai"
    message:"get {input} and put into {output}"
    shell:
        "samtools index {input}"


rule bcftools_call:
    input:
        fa="%s/genome.fa" % (PATHTOFILES),
        bam=expand("sorted_reads/{sample}.bam", sample=SAMPLES),
        bai=expand("sorted_reads/{sample}.bam.bai", sample=SAMPLES)
    output:
        "calls/all.vcf"
    message:" run pileup with {input.fa} and {input.bam} and write to {output}"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv - > {output}"

rule report:
    input:
        "calls/all.vcf"
    output:
        "out.html"
    message:"create ouput report html"
    run:
        from snakemake.utils import report
        with open(input[0]) as f:
            n_calls = sum(1 for line in f if not line.startswith("#"))

        report("""
        An example workflow
        ===================================

        Reads were mapped to the Yeas reference genome 
        and variants were called jointly with
        SAMtools/BCFtools.

        This resulted in {n_calls} variants (see Table T1_).
        """, output[0], metadata="Author: Mr Pipeline", T1=input[0])

