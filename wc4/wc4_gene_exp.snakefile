import os
import shutil
from snakemake.utils import R
PATHOFFILES= "/commons/Themas/Thema11/Dataprocessing/WC04/data"
#~ configfile: "wc4_config.yaml"


rule all:
	input:
		"gene_expression.png"

rule test_data_collect:
	run:
		if "data" in os.listdir():
			pass
		else:
			shell("mkdir data")
		source_files = os.listdir(PATHOFFILES)
		for source_file in source_files:
			shutil.copy(PATHOFFILES+"/"+source_file,"./data",follow_symlinks=False)

rule plot_gene_expression:
	input:
		"%s/gene_ex.csv" % PATHOFFILES
	output:
		"gene_expression.png"
	script:
		"wc4_gene_exp_script.R"

