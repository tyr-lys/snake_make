configfile:"speed_test.yaml"

rule all:
	input:
		expand("kaas{ding}.txt",ding=config["speed_test"])

rule my_test:
	input:
		"things/{ding}.txt"
	output:
		"kaas{ding}.txt"
	shell:
		"python3 speed_test_script.py {input}\n"
		"touch {output}"
