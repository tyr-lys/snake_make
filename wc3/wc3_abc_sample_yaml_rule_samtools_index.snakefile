rule samtools_index:
    input:
        "sorted_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam.bai"
    message:"get {input} and put into {output}"
    shell:
        "samtools index {input}"
