import os
from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider
configfile: "config.yaml"
HTTP = HTTPRemoteProvider()

rule all:
	input:
		expand("data/{sample}.bai",sample=config["samples"])

rule collect_data:
	input:
		HTTP.remote(expand("bioinf.nl/~fennaf/snakemake/WC03/data/{download_sample}.bam",download_sample = config["samples"]), keep_local=True)
	output:
		expand("data/{sample}.bam",sample=config["samples"])
	run:
		shell("mv {input} ./data")



rule build_bam_index:
    input:
        "data/{sample}.bam"
    output:
        "data/{sample}.bai"
    message: "Executing Picard  Buildbamindex with {threads} threads on the following files {input}."
    shell:
        "picard-tools BuildBamIndex I={input} VALIDATION_STRINGENCY=SILENT"
