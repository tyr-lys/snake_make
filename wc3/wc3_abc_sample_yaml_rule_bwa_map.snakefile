PATHTOFILES = '/commons/Themas/Thema11/Dataprocessing/data'
rule bwa_map:
    input:
        "{}/genome.fa".format(PATHTOFILES),
        "%s/samples/{sample}.fastq" % (PATHTOFILES)
    output:
        "mapped_reads/{sample}.bam"
    message: "executing bwa mem on the following {input} to generate the following {output}"
    shell:
        "bwa mem {input} | samtools view -Sb - > {output}"
